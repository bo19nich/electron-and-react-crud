import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Home from "./App";
import './index.css';
import About from "./pages/about";
import AddUser from './pages/AddUser.page';


ReactDOM.render(
    <Router>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/add" component={AddUser} />
            <Route exact path="/about/:id" component={About} />
        </Switch>
    </Router>,
    document.getElementById("root")

)
