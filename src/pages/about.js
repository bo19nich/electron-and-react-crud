import React, { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import InputField from "../components/inputFieldComponent/InputField.component"

const electron = window.require('electron')
const { ipcRenderer } = electron

const About = ({ match }) => {
    const username = match.params.id

    const [userState, setUserState] = useState({
        id: '',
        first_name: '',
        last_name: '',
        user_name: '',
        address: '',
        location: '',
        country: ''
    })

    const {
        first_name,
        last_name,
        user_name,
        address,
        location,
        country,
    } = userState

    useEffect(() => {
        ipcRenderer.send('Fetch:userId', { username })
        ipcRenderer.on("viewUser:res", function (e, userData) {
            setUserState(userData[0])
        })
    }, [])

    const handleChangeValue = e => {
        const { name, value } = e.target
        setUserState({ ...userState, [name]: value })
    }

    const handleSubmit = e => {
        e.preventDefault()
        ipcRenderer.send('update:user', userState)
        alert("User updated successfully!")
    }

    return (
        <div className="container">
            <h5>
                <Link to="/">Back</Link>
            </h5>
            <p>View User</p>
            <form onSubmit={handleSubmit}>
                <InputField
                    label="First Name"
                    type="text"
                    name="first_name"
                    isRequired={false}
                    handleOnChangeValue={handleChangeValue}
                    isValue={first_name}
                />
                <InputField
                    label="Last Name"
                    type="text"
                    name="last_name"
                    isRequired={false}
                    handleOnChangeValue={handleChangeValue}
                    isValue={last_name}
                />
                <InputField
                    label="User Name"
                    type="text"
                    name="user_name"
                    isRequired={false}
                    handleOnChangeValue={handleChangeValue}
                    isValue={user_name}
                />
                <InputField
                    label="Address"
                    type="text"
                    name="address"
                    isRequired={false}
                    handleOnChangeValue={handleChangeValue}
                    isValue={address}
                />
                <InputField
                    label="Location"
                    type="text"
                    name="location"
                    isRequired={false}
                    handleOnChangeValue={handleChangeValue}
                    isValue={location}
                />
                <InputField
                    label="Country"
                    type="text"
                    name="country"
                    isRequired={false}
                    handleOnChangeValue={handleChangeValue}
                    isValue={country}
                />
                <button type="submit" className="btn btn-warning">Save Changes</button>
            </form>
        </div>
    )
}

export default About