import React, { useState } from "react";
import { Link } from "react-router-dom";
import InputField from "../components/inputFieldComponent/InputField.component";

const electron = window.require("electron");
const { ipcRenderer } = electron;

export default function AddUser() {
  const [userState, setUserState] = useState({
    id: "",
    first_name: "",
    last_name: "",
    user_name: "",
    address: "",
    location: "",
    country: "",
  });

  const {
    first_name,
    last_name,
    user_name,
    address,
    location,
    country,
  } = userState;

  const handleChangeValue = (e) => {
    const { name, value } = e.target;
    setUserState({ ...userState, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    ipcRenderer.send("add:user", userState);
    alert("User added successfully!");
  };

  return (
    <div className="container">
      <h5>
        <Link to="/">Back</Link>
      </h5>
      <p>View User</p>
      <form onSubmit={handleSubmit}>
        <InputField
          label="First Name"
          type="text"
          name="first_name"
          isRequired={true}
          handleOnChangeValue={handleChangeValue}
          isValue={first_name}
        />
        <InputField
          label="Last Name"
          type="text"
          name="last_name"
          isRequired={true}
          handleOnChangeValue={handleChangeValue}
          isValue={last_name}
        />
        <InputField
          label="User Name"
          type="text"
          name="user_name"
          isRequired={true}
          handleOnChangeValue={handleChangeValue}
          isValue={user_name}
        />
        <InputField
          label="Address"
          type="text"
          name="address"
          isRequired={true}
          handleOnChangeValue={handleChangeValue}
          isValue={address}
        />
        <InputField
          label="Location"
          type="text"
          name="location"
          isRequired={true}
          handleOnChangeValue={handleChangeValue}
          isValue={location}
        />
        <InputField
          label="Country"
          type="text"
          name="country"
          isRequired={true}
          handleOnChangeValue={handleChangeValue}
          isValue={country}
        />
        <button type="submit" className="btn btn-warning">
          Add User
        </button>
      </form>
    </div>
  );
}
