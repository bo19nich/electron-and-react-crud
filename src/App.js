import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import './App.css';

const electron = window.require('electron')
const { ipcRenderer } = electron

const App = ({ history }) => {

  const [appState, setAppState] = useState({
    first_name: '',
    last_name: '',
    users: []
  })

  useEffect(() => {
    ipcRenderer.send("main:fetch")
    ipcRenderer.on("main:res", function (e, usersData) {
      setAppState({ ...appState, users: usersData })
    })
  }, [])
  const { users } = appState
  const renderNamesList = () => {

    console.log('users', users)
    return users.map((user, idx) => (
      <tr key={idx}>
        <th scope="row"> {(idx + 1)} </th>
        <td>{user.first_name}</td>
        <td>{user.last_name}</td>
        <td>{user.user_name}</td>
        <td>
          <button onClick={() => history.push(`/about/${user.user_name}`)} className="btn btn-warning">View</button>{" "}
          <button onClick={() => handleDeletUser(user.id)} className="btn btn-danger">Delete</button>
        </td>
      </tr>
    ))
  }

  const handleDeletUser = userId => {
    ipcRenderer.send("delete:user", { userId })
  }

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark"
        style={{ color: '#eee', textAlign: 'center', fontSize: 25, fontWeight: 700 }}
      >Simple crud App</nav>
      <Link className="btn btn-info my-5 mx-5" to="/add">
        Add User
        </Link>
      <div className="container">
        <h5>Users List</h5>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Username</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {renderNamesList()}

          </tbody>
        </table>
      </div>
    </div>
  );
}
export default App;
