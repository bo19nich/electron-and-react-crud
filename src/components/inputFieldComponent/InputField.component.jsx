import React from "react";

export default function InputField({
  type,
  name,
  handleOnChangeValue,
  isRequired,
  isValue,
  label,
}) {
  return (
    <div className="form-group">
      <label htmlFor={name}> {label} </label>
      <input
        type={type}
        name={name}
        onChange={handleOnChangeValue}
        defaultValue={isValue}
        required={isRequired}
        className="form-control"
      />
    </div>
  );
}
