const electron = require("electron");
const { app, BrowserWindow, Menu, ipcMain } = electron;
const path = require("path");
const isDev = require("electron-is-dev");
let mainWindow, createItemWindow;

var mysql = require('mysql');

// Add the credentials to access your database
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'electron_crud'
});

// connect to mysql
connection.connect(function (err) {
    // in case of error
    if (err) {
        console.log(err.code);
        console.log(err.fatal);
    }
});

let query

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 1200,
        height: 800,
        icon: "",
        webPreferences: {
            nodeIntegration: true,
        }
    });

    mainWindow.loadURL(
        isDev
            ? "http://localhost:3000"
            : `file://${path.join(__dirname, "../build/index.html")}`
    );
    // const mainMenu = Menu.buildFromTemplate(menuTemplate)
    // Menu.setApplicationMenu(mainMenu)

    mainWindow.once('ready-to-show', () => { mainWindow.show() })

    ipcMain.on('main:fetch', function () {
        // Fetch All Users from the database
        query = 'SELECT * FROM `user`';

        connection.query(query, function (err, rows, fields) {
            if (err) {
                console.log("An error ocurred performing the query.");
                console.log(err);
                return;
            }
            mainWindow.webContents.send("main:res", rows)
            // console.log("Query succesfully executed", rows);
        });
    })

    // INSERT NEW USER
    ipcMain.on('add:user', function (event, arg) {


        query = "INSERT INTO user  SET ?"
        connection.query(query, arg, function (err, rows, fields) {
            if (err) {
                console.log("An error ocurred performing the query.");
                console.log(err);
                return;
            } else {
                console.log("user created successfully")
            }
        });
    })

    // GET USER BY ID
    ipcMain.on('Fetch:userId', function (event, arg) {
        const { username } = arg

        query = 'SELECT * FROM `user` WHERE `user_name` = ?'
        connection.query(query, username, function (err, rows, fields) {
            if (err) {
                console.log("An error ocurred performing the query.");
                console.log(err);
                return;
            }
            mainWindow.webContents.send("viewUser:res", rows)
            console.log("Query succesfully executed", rows);
        });
    })

    // UPDATE USER
    ipcMain.on('update:user', function (event, arg) {
        const {
            id,
            first_name,
            last_name,
            user_name,
            address,
            location,
            country,
        } = arg

        query = "UPDATE user  SET first_name=?, last_name=?, user_name=?, address=?, location=?, country=? WHERE id = ?"
        connection.query(query, [first_name, last_name, user_name, address, location, country, id], function (err, rows, fields) {
            if (err) {
                console.log("An error ocurred performing the query.");
                console.log(err);
                return;
            } else {
                console.log("user updated successfully")
            }
        });
    })

    // DELETE USER channel delete:user
    ipcMain.on('delete:user', function (event, arg) {
        const { userId } = arg

        query = 'DELETE FROM user WHERE id = ?'
        connection.query(query, userId, function (err, rows, fields) {
            if (err) {
                console.log("An error ocurred performing the query.");
                console.log(err);
                return;
            } else {
                console.log("User deleted successfully")
            }
        });
    })

    mainWindow.on("closed", () => {
        // Close the connection When closing app
        connection.end(function () {
            // The connection has been closed
        });
        return (mainWindow = null)
    });
}

app.on("ready", createWindow);

// Handle create item window
// function createAddWindow() {
//     createItemWindow = new BrowserWindow({
//         width: 300,
//         height: 200,
//         icon: "",
//         parent: mainWindow
//     });

//     createItemWindow.loadURL(
//         isDev
//             ? "http://localhost:3000/about"
//             : `file://${path.join(__dirname, "../build/index.html")}`
//     );
// }

// Create Menu template
const menuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Quit',
                click() {
                    app.quit()
                }
            }
        ]
    },
    {
        label: 'Dev Tools',
        role: ['dev-tools']
    }
]

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    if (mainWindow === null) {
        createWindow();
    }
});